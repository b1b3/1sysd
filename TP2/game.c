#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));

    int nombreMystere = rand() % 100 + 1;
    int proposition, tentatives = 0;

    printf("Bienvenue dans le jeu ! Devinez le nombre mystere entre 1 et 100.\n");

    do {
        printf("Votre proposition : ");
        scanf("%d", &proposition);
        tentatives++;

        if (proposition == nombreMystere) {
            printf("Félicitations ! Vous avez trouvé le nombre mystere en %d tentatives.\n", tentatives);
        } else if (proposition < nombreMystere) {
            printf("Plus grand ! Essayez encore.\n");
        } else {
            printf("Plus petit ! Essayez encore.\n");
        }

    } while (proposition != nombreMystere);

    return 0;
}

