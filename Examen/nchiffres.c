#include <stdio.h>

int n_chiffres(char *s) {
    int count = 0;
    while (*s) {
        if (*s >= '0' && *s <= '9') {
            count++;
        }
        s++;
    }
    return count;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <chaine_de_caracteres>\n", argv[0]);
        return 1;
    }

    int nb_chiffres = n_chiffres(argv[1]);
    printf("%d\n", nb_chiffres);

    return 0;
}

