#include <stdio.h>

float inches2cm(float inches) {
    return inches * 2.54;
}

float cm2inches(float cm) {
    return cm / 2.54;
}

int main() {
    int choix;
    float valeur;

    printf("Que souhaitez-vous convertir ?\n");
    printf("1. Pouces vers centimètres\n");
    printf("2. Centimètres vers pouces\n");
    scanf("%d", &choix);

    if (choix == 1) {
        printf("Entrez une longueur en pouces : ");
        scanf("%f", &valeur);
        printf("%.2f pouces équivalent à %.2f centimètres.\n", valeur, inches2cm(valeur));
    } else if (choix == 2) {
        printf("Entrez une longueur en centimètres : ");
        scanf("%f", &valeur);
        printf("%.2f centimètres équivalent à %.2f pouces.\n", valeur, cm2inches(valeur));
    } else {
        printf("Choix invalide.\n");
        return 1;
    }

    return 0;
}

