#include <stdio.h>

void display_tab(int tab[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

int nb_uniq(int tab[], int size, int newtab[]) {
    int i, j;

    if (size == 0) {
        return 0; 
    }

    newtab[0] = tab[0];
    j = 1;

    for (i = 1; i < size; i++) {
        if (tab[i] != tab[i - 1]) {
            newtab[j] = tab[i];
            j++;
        }
    }

    return j;
}

int main() {
    int t1[] = { 1, 2, 5, 7, 10 };
    int t2[] = { 1, 1, 4, 5, 42, 42, 42, 4, 5, 42 }; 
    int new_t1[50], new_t2[50]; 
    int new_size;

    new_size = nb_uniq(t1, 5, new_t1);
    display_tab(new_t1, new_size); 
    new_size = nb_uniq(t2, 10, new_t2);
    display_tab(new_t2, new_size);

    return 0;
}

