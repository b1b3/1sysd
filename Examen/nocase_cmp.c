#include <stdio.h>

int nocase_equal(char *s1, char *s2) {
    while (*s1 != '\0' && *s2 != '\0') {
        
        if ((*s1 != *s2) && ((*s1 | 32) != (*s2 | 32))) {
            return 0;
        }
        s1++;
        s2++;
    }
    
    return *s1 == *s2;
}

int main() {
    char chaine1[100], chaine2[100];

    printf("Entrez la première chaîne : ");
    scanf("%s", chaine1);

    printf("Entrez la deuxième chaîne : ");
    scanf("%s", chaine2);

    if (nocase_equal(chaine1, chaine2)) {
        printf("identiques\n");
    } else {
        printf("différentes\n");
    }

    return 0;
}

