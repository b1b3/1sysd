#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node *next;
} node;

node *create_node(int data) {
    node *new_node = (node *)malloc(sizeof(node));
    if (new_node == NULL) {
        printf("Allocation de mémoire échouée.\n");
        exit(1);
    }
    new_node->data = data;
    new_node->next = NULL;
    return new_node;
}

void append_node(node **head, int data) {
    node *new_node = create_node(data);
    if (*head == NULL) {
        *head = new_node;
    } else {
        node *current = *head;
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = new_node;
    }
}

void display_list(node *head) {
    node *current = head;
    while (current != NULL) {
        printf("%d ", current->data);
        current = current->next;
    }
    printf("\n");
}

node *tab2list(int tab[], int size) {
    node *head = NULL;
    for (int i = 0; i < size; i++) {
        append_node(&head, tab[i]);
    }
    return head;
}

int list2tab(node *head, int tab[]) {
    int i = 0;
    node *current = head;
    while (current != NULL) {
        tab[i++] = current->data;
        current = current->next;
    }
    return i;
}

int main() {
    int tab[] = {1, 2, 3, 4, 5};
    int size = sizeof(tab) / sizeof(tab[0]);

    node *head = tab2list(tab, size);

    printf("Liste chaînée : ");
    display_list(head);

    int new_tab[size];
    int new_size = list2tab(head, new_tab);

    printf("Tableau : ");
    for (int i = 0; i < new_size; i++) {
        printf("%d ", new_tab[i]);
    }
    printf("\n");

    node *temp;
    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return 0;
}


